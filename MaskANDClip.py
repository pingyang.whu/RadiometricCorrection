import gdal
from gdalconst import GA_ReadOnly

from osgeo import gdal
gtif = gdal.Open("c:\\DEV\\FuelMapping\\registration\\Sample_Images\\L2A_T14RMT_20170130T171541_B04_10m.jp2")
print gtif
print gtif.GetMetadata()

ulx, xres, xskew, uly, yskew, yres = gtif.GetGeoTransform()
lrx = ulx + (gtif.RasterXSize * xres)
lry = uly + (gtif.RasterYSize * yres)
print ulx, xres, xskew, uly, yskew, yres

input_raster = "path/to/rgb.tif"
output_raster = "path/to/rgb_output_cut.tif"

ds = gdal.Warp(output_raster,
               input_raster,
               format='GTiff',
               cutlineDSName=input_kml,
               cutlineLayer='extent',
               dstNodata=0)

"""
data = gdal.Open('img_mask.tif', GA_ReadOnly)
geoTransform = data.GetGeoTransform()
minx = geoTransform[0]
maxy = geoTransform[3]
maxx = minx + geoTransform[1] * data.RasterXSize
miny = maxy + geoTransform[5] * data.RasterYSize
call('gdal_translate -projwin ' + ' '.join([str(x) for x in [minx, maxy, maxx, miny]]) + ' -of GTiff img_orig.tif img_out.tif', shell=True)


# GDAL_Calc.py - A Mask.tif - B CutBigImageToClip.tif - -outfile = SmallerFile.tif - -NoDataValue = 0 - -Calc = "B*(A>0)"
# GDAL_Translate - of GTIFF - projwin ulx uly lrx lry BigImageToClip.tif CutBigImageToClip.tif
# gdalwarp - te xmin ymin xmax ymax - tr 3 3 - r bilinear A_state_30m.tif C_county_3m.tif
# gdal_calc.py -A L2A_T14RMT_20170130T171541_B04_10m.jp2 -B L2A_T14RMT_20170430T171301_B04_10m.jp2 --outfile = result.tif --calc = "B*(A>0)"

fileNameIn = "/tmp/geotiff/Global_taxonomic_richness_of_soil_fungi.tif"
fileNameOut = "/tmp/geotiff/Global_taxonomic_richness_of_soil_fungi.tiff"
dst_options = ['COMPRESS=DEFLATE', "PREDICTOR=3", "TILED=YES"]
noDataValue = -3.4028234663852886e+38

from osgeo import gdal
import numpy

src_ds = gdal.Open(fileNameIn)
format = "GTiff"
driver = gdal.GetDriverByName(format)
dst_ds = driver.CreateCopy(fileNameOut, src_ds, False, dst_options)

# Set location
dst_ds.SetGeoTransform(src_ds.GetGeoTransform())
# Set projection
dst_ds.SetProjection(src_ds.GetProjection())
srcband = src_ds.GetRasterBand(1)

dataraster = srcband.ReadAsArray().astype(numpy.float)
#Rplace the nan value with the predefiend noDataValue
dataraster[numpy.isnan(dataraster)] = noDataValue

dst_ds.GetRasterBand(1).WriteArray(dataraster)
dst_ds.GetRasterBand(1).SetNoDataValue(noDataValue)

dst_ds = None
"""
